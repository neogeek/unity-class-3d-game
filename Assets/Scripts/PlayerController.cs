﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	public LayerMask groundLayerMask;
	public Transform groundChecker;

	private float speed = 6.0f;
	private float raycastDistance = 100.0f;
	private float jumpForce = 500.0f;

	private float h;
	private float v;
	private float verticalSpeed = 0.0f;

	private bool grounded = true;
	private bool isWalking = false;
	private bool isDead = false;

	private Vector3 movement;
	private Animator animator;
	private Rigidbody rb;

	void Awake () {

		animator = gameObject.GetComponent<Animator>();
		rb = gameObject.GetComponent<Rigidbody>();

	}

	void Update () {

		v = Input.GetAxisRaw("Vertical");
		h = Input.GetAxisRaw("Horizontal");

		if (Input.GetKeyDown("space") && grounded) {

			rb.AddForce(new Vector3(0, jumpForce, 0));

		}

		verticalSpeed = rb.velocity.y;

		if (Input.GetKeyDown("e")) {

			isDead = true;
			animator.SetTrigger("dead");

		}

	}

	void FixedUpdate () {

		if (!isDead) {

			Move();
			Turn();
			Animate();
			CheckGrounded();

		}

	}

	void Move () {

		isWalking = h != 0 || v != 0;

		movement.Set(h, 0, v);
		movement = movement.normalized * speed * Time.deltaTime;

		rb.MovePosition(gameObject.transform.position + movement);

	}

	void Turn () {

		Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit floorHit;

		if (Physics.Raycast(cameraRay, out floorHit, raycastDistance, groundLayerMask)) {

			var playerToMouse = floorHit.point - gameObject.transform.position;

			playerToMouse.y = 0;

			Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

			rb.MoveRotation(newRotation);

		}

	}

	void Animate () {

		animator.SetBool("isWalking", isWalking);
		animator.SetBool("grounded", grounded);
		animator.SetFloat("verticalSpeed", verticalSpeed);

	}

	void CheckGrounded () {

		grounded = Physics.Raycast(groundChecker.position, new Vector3(0, -1, 0), 1f, groundLayerMask);

	}

}
